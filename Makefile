CC = gcc
EXE = mysh
CFLAGS = -g -o $(EXE)
SRC = mysh.c

all:
	$(CC) $(CFLAGS) $(SRC)
	
run:
	./$(EXE)

clean:
	rm -rf $(EXE)
